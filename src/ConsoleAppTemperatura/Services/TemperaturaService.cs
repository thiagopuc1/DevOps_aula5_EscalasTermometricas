﻿namespace Services
{
    public class TemperaturaService
    {
        public decimal ConversorKelvinParaCelsius(decimal temperaturaKelvin)
        {
            return temperaturaKelvin - 273.15M;
        }

        public decimal ConversorCelsiusParaKelvin(decimal temperaturaCelsius)
        {
            return temperaturaCelsius + 273.15M;
        }

        public decimal ConversorFahrenheitParaCelsius(decimal temperaturaFahrenheit)
        {
            return (temperaturaFahrenheit - 32M) / 1.8M;
        }

        public decimal ConversorCelsiusParaFahrenheit(decimal temperaturaCelsius)
        {
            return (temperaturaCelsius * 1.8M) + 32M;
        }

        public decimal ConversorFahrenheitParaKelvin(decimal temperaturaFahrenheit)
        {
            return ConversorCelsiusParaKelvin(ConversorFahrenheitParaCelsius(temperaturaFahrenheit));
        }

        public decimal ConversorKelvinParaFahrenheit(decimal temperaturaKelvin)
        {
            return ConversorCelsiusParaFahrenheit(ConversorKelvinParaCelsius(temperaturaKelvin));
        }
    }
}
