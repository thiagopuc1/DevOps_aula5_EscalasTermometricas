﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TemperaturaController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get(decimal temperatura)
        {
            var servico = new TemperaturaService();
            return Ok(servico.ConversorCelsiusParaKelvin(temperatura));
        }

        [HttpGet]
        public IActionResult Get2(decimal temperatura)
        {
            var servico = new TemperaturaService();
            return Ok(servico.ConversorCelsiusParaKelvin(temperatura));
        }
    }
}
