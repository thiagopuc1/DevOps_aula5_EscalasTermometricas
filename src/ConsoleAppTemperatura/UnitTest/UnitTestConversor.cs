using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;

namespace UnitTest
{
    [TestClass]
    public class UnitTestConversor
    {
        [TestMethod]
        public void TestCelsiusToKelvin_Sucess()
        {
            var servico = new TemperaturaService();

            var kelvin = servico.ConversorCelsiusParaKelvin(0);

            Assert.AreEqual(273.15M, kelvin);
        }
    }
}
